angular.module('chitchat.controllers', [])

.controller('LoginCtrl', function($scope, $state, $q, UserService, $ionicLoading, FACEBOOK_APP_ID) {

//This is the success callback from the login method
  var fbLoginSuccess = function(response) {
    if (!response.authResponse){
      fbLoginError("Cannot find the authResponse");
      return;
    }
    console.log('fbLoginSuccess');

    var authResponse = response.authResponse;

    getFacebookProfileInfo(authResponse)
    .then(function(profileInfo) {

      console.log('profile info success', profileInfo);
      //for the purpose of this example I will store user data on local storage
      UserService.setUser({
        acess_token : authResponse.accessToken,
        id: profileInfo.id,
        gender : profileInfo.gender,
        name: profileInfo.first_name,
        picture : "http://graph.facebook.com/" + profileInfo.id + "/picture?type=square&width=9999&height=9999"
      });
      
      $scope.socket.emit('update_user', UserService.getUser());

      $ionicLoading.hide();
      $state.go('app.home');
    }, function(fail){
      //fail get profile info
      console.log('profile info fail', fail);
    });
  };

  //This is the fail callback from the login method
  var fbLoginError = function(error){
    console.log('fbLoginError');
    $ionicLoading.hide();
  };

  //this method is to get the user profile info from the facebook api
  var getFacebookProfileInfo = function (authResponse) {
    var info = $q.defer();

    facebookConnectPlugin.api('/me?fields=gender,birthday,first_name&access_token=' + authResponse.accessToken, null,
      function (response) {
        console.log(response);
        info.resolve(response);
      },
      function (response) {
        info.reject(response);
      }
    );
    return info.promise;
  }

  //This method is executed when the user press the "Login with facebook" button
  $scope.login = function() {
    if (!window.cordova) { alert('not cordova');
      //this is for browser only
      facebookConnectPlugin.browserInit(FACEBOOK_APP_ID);
    }
    alert(facebookConnectPlugin);
    facebookConnectPlugin.getLoginStatus(function(success){
     if(success.status === 'connected'){
        // the user is logged in and has authenticated your app, and response.authResponse supplies
        // the user's ID, a valid access token, a signed request, and the time the access token
        // and signed request each expire
        console.log('getLoginStatus',success.status);
        $state.go('app.home');
     } else {
        //if (success.status === 'not_authorized') the user is logged in to Facebook, but has not authenticated your app
        //else The person is not logged into Facebook, so we're not sure if they are logged into this app or not.
        console.log('getLoginStatus',success.status);
        $ionicLoading.show({
          template: 'Logging in...'
        });

        //ask the permissions you need. You can learn more about FB permissions here: https://developers.facebook.com/docs/facebook-login/permissions/v2.4
        facebookConnectPlugin.login(['email', 'public_profile', 'user_about_me', 'user_hometown', 'user_likes', 'user_location', 'user_birthday', 'user_photos'], fbLoginSuccess, fbLoginError);
      }
    });
  }
})

// .controller('FeedCtrl', function($scope, $state, UserService, $q, FACEBOOK_APP_ID, $ionicLoading) {
//   $scope.user = UserService.getUser();
//   var authResponse = $scope.user.authResponse;

//   $scope.doRefresh = function() {
//     var feed = $q.defer();

//     if (!window.cordova) {
//       //this is for browser only
//       facebookConnectPlugin.browserInit(FACEBOOK_APP_ID);
//     }

//     $ionicLoading.show({
//       template: 'One moment...'
//     });

//     facebookConnectPlugin.api('/' + authResponse.userID + '/feed?fields=from,full_picture,created_time,message,story,comments,likes,place&access_token=' + authResponse.accessToken, null,
//       function (response) {
//         feed.resolve(response.data);
//       },
//       function (response) {
//         feed.reject(response);
//       }
//     );

//     feed.promise.then(function(feed_response){
//       $scope.items = feed_response;
//       $ionicLoading.hide();
//       $scope.$broadcast('scroll.refreshComplete');
//     }, function(fail){
//       $ionicLoading.hide();
//       $scope.$broadcast('scroll.refreshComplete');
//       $state.go('login');
//     });
//   };

//   $scope.doRefresh();
// })



.controller('AppCtrl', function($scope, $state, $ionicPopup, UserService, $ionicLoading, FACEBOOK_APP_ID) {
  
   $scope.user = UserService.getUser();

   // LOG OUT
   // A confirm dialog to be displayed when the user wants to log out
   $scope.showConfirmLogOut = function() {
     var confirmPopup = $ionicPopup.confirm({
       title: 'Log out',
       template: 'Are you sure you want to log out?'
     });
     confirmPopup.then(function(res) {
       if(res) {
         //logout
         $ionicLoading.show({
           template: 'Logging out...'
         });

         if (!window.cordova) {
           //this is for browser only
           facebookConnectPlugin.browserInit(FACEBOOK_APP_ID);
         }

         facebookConnectPlugin.logout(function(){
           //success
           $ionicLoading.hide();
           $state.go('login');
         },
         function(fail){
           $ionicLoading.hide();
         });
       } else {
        //cancel log out
       }
     });
   };
})

.controller('HomeCtrl', function($scope, $state, UserService) {
  console.log('home');
  $('.search_btn').click(function() {

    $(this).find('i').fadeOut('fast');
    $(this).toggle('scale', function() {

      $('.search_pic').toggle('scale');

      setTimeout(function() {
        $('.water').html('<div class="wave time1"></div> \
              <div class="wave time2"></div> \
              <div class="wave time3"></div> \
              <div class="wave time4"></div>');
      }, 200);

      search();
    });

    return false;
  });

  function search() {
    
  }

})

.controller('ChatCtrl', function($scope, $rootScope, $state, UserService) {
  console.log('hi');
  if($scope.room_id == undefined || $scope.room_id == 0) {
    $state.go('app.home');
  }

  $('.toggle_profile_view').click(function() {
    if ($(".profile_details_wrapper").is(":visible")) {
        $(".profile_details").animate(
            {
                opacity: "0"
            },
            200,
            function(){
                $(".profile_details_wrapper").slideUp(300);
            }
        );
    } else {
        $(".profile_details_wrapper").slideDown(300, function(){
            $(".profile_details").animate(
                {
                    opacity: "1"
                },
                200
            );
        });
    } 

    return false;
  });

  var window_height = $(window).height();
  //$('.profile_preview').css('max-height', window_height + 'px');

  $(window).resize(function() {
    var window_height = $(window).height();
    //$('.profile_preview').css('max-height', window_height + 'px');
  });


  //Dummy message send
  $('#send_message').click(function() {

      $('.chat_wrapper').append('<div class=""> \
                    <img ng-click="viewProfile(message)" class="profile-pic left" ng-src="http://ionicframework.com/img/docs/venkman.jpg" onerror="onProfilePicError(this)" src="http://ionicframework.com/img/docs/venkman.jpg"> \
                      <div class="chat-bubble left"> \
                          <div class="message ng-binding" ng-bind-html="message.text | nl2br" autolinker="">Supp dudeeeee.....</div> \
                          <div class="message-detail"> \
                              <span ng-click="viewProfile(message)" class="bold ng-binding">Venkman</span>, \
                              <span am-time-ago="message.date">2 minutes ago</span> \
                          </div> \
                      </div> \
                  </div> \
                  <div class=""> \
                    <img ng-click="viewProfile(message)" class="profile-pic left" ng-src="http://ionicframework.com/img/docs/mcfly.jpg" onerror="onProfilePicError(this)" src="http://ionicframework.com/img/docs/mcfly.jpg"> \
                      <div class="chat-bubble right"> \
                          <div class="message ng-binding" ng-bind-html="message.text | nl2br" autolinker="">Yooooooooo!</div> \
                          <div class="message-detail"> \
                              <span ng-click="viewProfile(message)" class="bold ng-binding">McFly</span>, \
                              <span am-time-ago="message.date">2 minutes ago</span> \
                          </div> \
                      </div> \
                  </div>');

    $('.chat_wrapper').animate({ scrollTop: $('.chat_wrapper').prop('scrollHeight') }, 300);

    return false;
  });


  //var socket = io.connect('http://162.243.89.138:8082', {'secure': true, 'reconnect': true, 'reconnection delay': 1000});
    var typing_active = false;
    var typing_timeout;

    //init new connection
    (function() {

        var user = UserService.getUser();

        $rootScope.socket.emit('online', user.id);
    
    })();


    //route chat request
    $rootScope.socket.on('nobody_available', function () {

        $('.chat-form').html('No one is free to chat. <a href="#" class="new_search">Search again</a>.');
    });


    //route chat request
    $rootScope.socket.on('chat_request', function (other_user_id) {

        $rootScope.socket.emit('accept_chat_request', other_user_id);
    });

    //route join private room
    $rootScope.socket.on('room_ready', function (room_id) {

        $rootScope.room_id = room_id;

        $rootScope.socket.emit('join_private_room', room_id);
    });

    //handle private chat start
    $rootScope.socket.on('private_chat_started', function (user_attr) {

        var html = '<li class="system_msg">' +
                 '<img class="avatar" alt="" src="' + user_attr.picture + '" />' +
                 '<div class="message">' +
                    'You are now chatting with ' + user_attr.name + ', ' + user_attr.age + ', ' + user_attr.gender + '. Say something!' +
                 '</div>' +
              '</li>';


        $('.chats').append(html);

        $('.chat-form').html('<div class="input-cont"> \
                    <textarea class="m-wrap message_text" placeholder="Type a message..."></textarea> \
                </div> \
                <div class="btn-cont"> \
                    <a href="" class="btn light-blue icn-only send_message"><i class="icon-ok"></i> Send</a> \
                    <a href="" class="btn light-blue icn-only leave_chat"><i class="icon-ok"></i> Leave</a> \
                </div>');
    });

    $rootScope.socket.on('is_typing', function (user_attr) {

        var html = '<li class="typing">' +
                 '<div class="message">' +
                    user_attr.name + ' is typing...' +
                 '</div>' +
              '</li>';

        $('.chats').append(html);
    });

    $rootScope.socket.on('stopped_typing', function () {

        $('li.typing').remove();
    });

    $rootScope.socket.on('user_left', function (user_attr) {

        var html = '<li class="system_msg">' +
                 '<img class="avatar" alt="" src="' + user_attr.picture + '" />' +
                 '<div class="message">' +
                    user_attr.name + ' has left the room.' +
                 '</div>' +
              '</li>';

        $('.chats').append(html);

        $('.chat-form').html('<a href="" class="btn light-blue icn-only new_search"><i class="icon-ok"></i> Search for new user</a>');
    });


    //new_search
    $('body').on('click', '.new_search', function() {

        $('.chats').html('');
        $('.chat-form').html('Searching...');

        var user_id = $('.from').val();

        $rootScope.socket.emit('online', user_id); //start cycle

        return false;
    });


    //leave chat
    $('body').on('click', '.leave_chat', function() {

        $rootScope.socket.emit('leave_private_room');

        $('.room_id').val('');

        return false;
    });

    //chat closed confirmation
    $rootScope.socket.on('chat_closed', function () { //from 'leave_private_room' event

        $('.chats').html('');
        $('.chat-form').html('Searching...');

        $('.room_id').val('');

        var user_id = $('.from').val();

        $rootScope.socket.emit('online', user_id); //start cycle
    });


    //send new message
    $('body').on('click', '.send_message', function() {
        
        sendMessage();

        return false;
    });


    $(document).keydown(function(e) {

        if($('.message_text').is(':focus')) {

            var code = (e.keyCode ? e.keyCode : e.which);

            if (code == 13) {

                sendMessage();
            
            } else if(code != 8) { //except backspace

                if(typing_active != true) {

                    var room_id = $('.room_id').val();

                    $rootScope.socket.emit('typing', room_id);

                }

                //----------------------------

                typing_active = true;

                clearTimeout(typing_timeout);

                typing_timeout = setTimeout(function() {

                    typing_active = false;

                    var room_id = $('.room_id').val();

                    $rootScope.socket.emit('stopped_typing', room_id);

                }, 1000);
            }
        }
    });


    function sendMessage() {

        var message_object = new Object;

        var room_id = $('.room_id').val();

        var message_text = stripHTML($(".message_text").val()); //stripHTML is a function
        message_text = message_text.replace(/\n/g, '<br />');

        message_text = message_text.trim();

        if(message_text != "") {
            message_text = linkify(message_text);

            message_object.message = message_text;
            message_object.room_id = room_id;

            $rootScope.socket.emit('send', message_object);

            $('.message_text').val("");
        }
    }


    //incoming messages
    $rootScope.socket.on('message', function (data) {

        if(data.message) {

            var month=new Array();
            month[0]="Jan";
            month[1]="Feb";
            month[2]="Mar";
            month[3]="Apr";
            month[4]="May";
            month[5]="Jun";
            month[6]="Jul";
            month[7]="Aug";
            month[8]="Sep";
            month[9]="Oct";
            month[10]="Nov";
            month[11]="Dec";

            var datetime = new Date();

            var cur_date = datetime.getDate();
            var cur_month = month[datetime.getMonth()];
            var cur_year = datetime.getFullYear();
            var cur_hour = datetime.getHours(); //gmt_offset goes here
            var cur_minute = datetime.getMinutes();

            var ampm = "AM";

            if(cur_minute < 10) {
                cur_minute = "0" + cur_minute;
            }

            if (cur_hour > 12) {
                cur_hour -= 12;
                ampm = "PM";
            } else if (cur_hour === 0) {
               cur_hour = 12;
               //Stays as AM.
            }


            var date_string = cur_month + " " + cur_date + ", " + cur_year;
            var time_string =  cur_hour + ":" + cur_minute;

            var li_class;
            
            var html = '<li>' +
                     '<img class="avatar" alt="" src="' + data.image + '" />' +
                     '<div class="message">' +
                        '<span class="arrow"></span>' +
                        '<a href="#" class="name">' + data.name + '</a>' +
                        '<span class="datetime"> on ' + date_string + ' at ' + time_string + ampm + '</span>' +
                        '<span class="body">' +
                        data.message + 
                        '</span>' +
                     '</div>' +
                  '</li>';


            $('.chats').append(html);


        } else {
            console.log("There is a problem:", data);
        }
    });


    function linkify(text) {  
        var urlRegex =/(\b(https?:\/\/|ftp:\/\/|file:\/\/|www.)[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;  
        return text.replace(urlRegex, function(url) {  
            return '<a href="' + url + '" target="_blank">' + url + '</a>';  
        })  
    }


    function stripHTML(string) {
       var tmp = document.createElement("DIV");
       tmp.innerHTML = string;
       return tmp.textContent || tmp.innerText || "";
    }

})

.controller('PreferencesCtrl', function($scope, $state, UserService) {
  $scope.user = UserService.getUser();

  var pref_gender = window.localStorage['user']['pref_gender'];
  var pref_min_age = window.localStorage['user']['pref_min_age'];
  var pref_max_age = window.localStorage['user']['pref_max_age'];

  if(pref_min_age == undefined || pref_max_age == undefined || pref_min_age > pref_max_age) {
    //somesing wong, set defaults
    pref_min_age = 20;
    pref_max_age = 30;
  }

  function gender_check() {
      //Gender
      var men = $('#men').is(':checked');
      var women = $('#women').is(':checked');

      if(men && women) {

        $('.gender').html('Men, Women');
        pref_gender = 'Men, Women';
        window.localStorage['user']['pref_gender'] = 'Men, Women';
      
      } else if(men) {

        $('.gender').html('Men');
        pref_gender = 'Men';
        window.localStorage['user']['pref_gender'] = 'Men';

      } else if(women) {

        $('.gender').html('Women');
        pref_gender = 'Men';
        window.localStorage['user']['pref_gender'] = 'Men';
      }
  }

  gender_check();

  $('body').on('change',  '#men, #women', function() {

    var men = $('#men').is(':checked');
    var women = $('#women').is(':checked');

    if(!men && !women) {

      var this_gender = $(this).attr('id');

      if(this_gender == 'men') {

        $('#women').prop('checked', true);

      } else if(this_gender == 'women') {

        $('#men').prop('checked', true);
      }
    }

    gender_check();
  });

  //Age range
  var slider = $('#age_range_slider')[0];

  noUiSlider.create(slider, {
    start: [ pref_min_age, pref_max_age ],
    step: 1,
    connect: true, // Display a colored bar between the handles
    behaviour: 'tap-drag',
    range: {
      'min': 18,
      'max': 55
    }
  });

  slider.noUiSlider.on('update', function(values, handle) {

    var lower_handle = parseInt(values[0]);
    var upper_handle = parseInt(values[1]);

    if(lower_handle == upper_handle) {

      var age_range = lower_handle;
    
    } else if(upper_handle == 55) {

      var age_range = lower_handle + ' - ' + upper_handle + '+';
    
    } else {

      var age_range = lower_handle + ' - ' + upper_handle;
    }

    $('.age_range').html(age_range);

    window.localStorage['user'].pref_min_age = lower_handle;
    window.localStorage['user'].pref_max_age = upper_handle;
    
  });

  $scope.$on('$locationChangeStart', function( event ) {

    var preferences = {pref_min_age: lower_handle, pref_max_age: upper_handle, pref_gender: pref_gender};

    $scope.socket.emit('update_preferences', preferences);

  });

})

// .controller('LikesCtrl', function($scope, $state, UserService, FACEBOOK_APP_ID, $q, $ionicLoading) {
//   $scope.user = UserService.getUser();
//   var authResponse = $scope.user.authResponse;

//   $scope.doRefresh = function() {
//     var likes = $q.defer();

//     if (!window.cordova) {
//       //this is for browser only
//       facebookConnectPlugin.browserInit(FACEBOOK_APP_ID);
//     }

//     $ionicLoading.show({
//       template: 'Loading likes...'
//     });

//     facebookConnectPlugin.api('/' + authResponse.userID + '/likes?access_token=' + authResponse.accessToken, null,
//     function (response) {
//       likes.resolve(response.data);
//     },
//     function (response) {
//       likes.reject(response);
//     });

//     likes.promise.then(function(photos){
//       $scope.likes = photos;
//       $ionicLoading.hide();
//       $scope.$broadcast('scroll.refreshComplete');
//     },function(fail){
//       $ionicLoading.hide();
//       $scope.$broadcast('scroll.refreshComplete');
//     });
//   };

//   $scope.doRefresh();
// })

.controller('ShareCtrl', function($scope, $state, UserService, $q, FACEBOOK_APP_ID, $ionicLoading) {
  $scope.user = UserService.getUser();
  $scope.image_to_share = "https://c1.staticflickr.com/9/8322/8057495684_335ee78565_z.jpg";

  if (!window.cordova) {
    //this is for browser only
    facebookConnectPlugin.browserInit(FACEBOOK_APP_ID);
  }

  // $scope.post_status = function() {
  //   facebookConnectPlugin.showDialog({
  //     method:"feed"
  //   },
  //   function (response) {
  //     $ionicLoading.show({ template: 'Status posted!', noBackdrop: true, duration: 2000 });
  //   },
  //   function (response) {
  //     //fail
  //   });
  // };

  $scope.send_message = function() {
    facebookConnectPlugin.showDialog({
      method: 'send',
      link:'http://example.com',
    },
    function (response) {
      $ionicLoading.show({ template: 'Message sent!', noBackdrop: true, duration: 2000 });
    },
    function (response) {
      //fail
    });
  };

  // $scope.post_image = function() {
  //   facebookConnectPlugin.showDialog(
  //   {
  //     method: "feed",
  //     picture: $scope.image_to_share,
  //     name:'Test Post',
  //     message:'This is a test post',
  //     caption: 'Testing using IonFB app',
  //     description: 'Posting photo using IonFB app'
  //   },
  //   function (response) {
  //     $ionicLoading.show({ template: 'Image posted!', noBackdrop: true, duration: 2000 });
  //   },
  //   function (response) {
  //     //fail
  //   });
  // };

  // $scope.share_link = function() {
  //   facebookConnectPlugin.showDialog(
  //   {
  //     method: "share",
  //     href: 'http://example.com',
  //   },
  //   function (response) {
  //     $ionicLoading.show({ template: 'Link shared!', noBackdrop: true, duration: 2000 });
  //   },
  //   function (response) {
  //     //fail
  //   });
  // };
});
