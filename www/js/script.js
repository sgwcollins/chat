$(document).ready(function(){

    function gender_check() {
	    //Gender
	    var men = $('#men').is(':checked');
	    var women = $('#women').is(':checked');

	    if(men && women) {

	    	$('.gender').html('Men, Women');
	    
	    } else if(men) {

	    	$('.gender').html('Men');

	    } else if(women) {

	    	$('.gender').html('Women');
	    }
	}

	gender_check();

	$('body').on('change',  '#men, #women', function() {

		var men = $('#men').is(':checked');
	    var women = $('#women').is(':checked');

		if(!men && !women) {

			var this_gender = $(this).attr('id');

			if(this_gender == 'men') {

				$('#women').prop('checked', true);

			} else if(this_gender == 'women') {

				$('#men').prop('checked', true);
			}
		}

		gender_check();
	});

    //Age range
 //    var slider = $('#age_range_slider')[0];

 //    noUiSlider.create(slider, {
	// 	start: [ 20, 30 ],
	// 	step: 1,
	// 	connect: true, // Display a colored bar between the handles
	// 	behaviour: 'tap-drag',
	// 	range: {
	// 		'min': 18,
	// 		'max': 55
	// 	}
	// });

	// slider.noUiSlider.on('update', function(values, handle) {

	// 	var lower_handle = parseInt(values[0]);
	// 	var upper_handle = parseInt(values[1]);

	// 	if(lower_handle == upper_handle) {

	// 		var age_range = lower_handle;
		
	// 	} else if(upper_handle == 55) {

	// 		var age_range = lower_handle + ' - ' + upper_handle + '+';
		
	// 	} else {

	// 		var age_range = lower_handle + ' - ' + upper_handle;
	// 	}

	// 	$('.age_range').html(age_range);
	// });
});     