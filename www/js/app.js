// angular.module is a global place for creating, registering and retrieving Angular modules
// 'chitchat' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('chitchat', ['ionic', 'chitchat.controllers', 'chitchat.services'])

.run(function($ionicPlatform, $rootScope, $state, UserService, FACEBOOK_APP_ID) {

  $ionicPlatform.on('deviceready', function(){

    cordova.plugins.backgroundMode.enable();

    cordova.plugins.backgroundMode.onactivate = function () {
      alert();
    };

    if (!window.cordova) {
      //this is for browser only
      facebookConnectPlugin.browserInit(FACEBOOK_APP_ID);
    }

    // facebookConnectPlugin.getLoginStatus(function(success){
    //  if(success.status === 'connected'){
        
    //     //console.log('connected!');
    //     $state.go('app.home');
    //  } else {
        
    //     //not logged in
    //     $state.go('login');
    //   }
    // });

    $rootScope.socket = 1;//io.connect('http://162.243.89.138:8082', {'secure': true, 'reconnect': true, 'reconnection delay': 1000});

  });

  // $ionicPlatform.ready(function(){
  //   facebookConnectPlugin.getLoginStatus(function(success){
  //     if((success.status === 'connected') && (UserService.userIsLoggedIn() === true)){
  //       $state.go('app.home');
  //     }else{
  //       $state.go('login');
  //     }
  //   });

  //   // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
  //   // for form inputs)
  //   if(window.cordova && window.cordova.plugins.Keyboard) {
  //     cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
  //   }
  //   if(window.StatusBar) {
  //     StatusBar.styleDefault();
  //   }
  // });

  // $ionicPlatform.on("resume", function(){
  //   facebookConnectPlugin.getLoginStatus(function(success){
  //     if((success.status != 'connected') || (UserService.userIsLoggedIn() === false)){
  //       $state.go('login');
  //     }
  //   });
  // });

    // UI Router Authentication Check
  // $rootScope.$on("$stateChangeStart", function(event, toState, toParams, fromState, fromParams){
  //   if (toState.data.authenticate) {
  //     facebookConnectPlugin.getLoginStatus(function(success){
  //       if((success.status === 'connected') && (UserService.userIsLoggedIn() === true)){
  //         //go ahead
  //       }else{
  //         event.preventDefault();
  //         $state.go('login');
  //       }
  //     },
  //     function(fail){
  //     });
  //   }
  // });
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

  .state('app', {
    url: "/app",
    abstract: true,
    templateUrl: "templates/menu.html",
    controller: 'AppCtrl'
  })

  .state('login', {
    url: "/",
    templateUrl: "templates/login.html",
    controller: 'LoginCtrl',
    data: {
      authenticate: false
    }
  })

  // .state('app.profile', {
  //   url: "/profile",
  //   views: {
  //     'menuContent': {
  //       templateUrl: "templates/profile.html",
  //       controller: 'ProfileCtrl'
  //     }
  //   },
  //   data: {
  //     authenticate: true
  //   }
  // })

  .state('app.home', {
    url: "/home",
    views: {
      'menuContent': {
        templateUrl: "templates/home.html",
        controller: 'HomeCtrl'
      }
    },
    data: {
      authenticate: true
    }
  })

  .state('app.chat', {
    url: "/chat",
    views: {
      'menuContent': {
        templateUrl: "templates/chat.html",
        controller: 'ChatCtrl'
      }
    },
    data: {
      authenticate: true
    }
  })

  .state('app.preferences', {
    url: "/preferences",
    views: {
      'menuContent': {
        templateUrl: "templates/preferences.html",
        controller: 'PreferencesCtrl'
      }
    },
    data: {
      authenticate: true
    }
  })

  // .state('app.likes', {
  //   url: "/likes",
  //   views: {
  //     'menuContent': {
  //       templateUrl: "templates/likes.html",
  //       controller: 'LikesCtrl'
  //     }
  //   },
  //   data: {
  //     authenticate: true
  //   }
  // })

  // .state('app.feed', {
  //   url: "/feed",
  //   views: {
  //     'menuContent': {
  //       templateUrl: "templates/feed.html",
  //       controller: 'FeedCtrl'
  //     }
  //   },
  //   data: {
  //     authenticate: true
  //   }
  // })

  // .state('app.share', {
  //   url: "/share",
  //   views: {
  //     'menuContent': {
  //       templateUrl: "templates/share.html",
  //       controller: 'ShareCtrl'
  //     }
  //   },
  //   data: {
  //     authenticate: true
  //   }
  // })

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/');
});
