angular.module('chitchat.services', [])

.constant("FACEBOOK_APP_ID", "612372758905222")

.service('UserService', function() {

//for the purpose of this example I will store user data on ionic local storage but you should save it on a database

  var setUser = function(user_data) {
    window.localStorage['user'] = JSON.stringify(user_data);
  };

  var userIsLoggedIn = function(){
    var user = getUser();
    return user.id != null;
  };

  var getUser = function(){
    return JSON.parse(window.localStorage['user'] || '{}');
  };

  return {
    getUser: getUser,
    setUser : setUser,
    userIsLoggedIn : userIsLoggedIn
  }
});
