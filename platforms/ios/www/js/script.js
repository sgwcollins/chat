$(document).ready(function(){

    function gender_check() {
	    //Gender
	    var men = $('#men').is(':checked');
	    var women = $('#women').is(':checked');

	    if(men && women) {

	    	$('.gender').html('Men, Women');
	    
	    } else if(men) {

	    	$('.gender').html('Men');

	    } else if(women) {

	    	$('.gender').html('Women');
	    }
	}

	gender_check();

	$('body').on('change',  '#men, #women', function() {

		var men = $('#men').is(':checked');
	    var women = $('#women').is(':checked');

		if(!men && !women) {

			var this_gender = $(this).attr('id');

			if(this_gender == 'men') {

				$('#women').prop('checked', true);

			} else if(this_gender == 'women') {

				$('#men').prop('checked', true);
			}
		}

		gender_check();
	});

});     